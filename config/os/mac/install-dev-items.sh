#!/bin/bash

# dev lang
brew install python@3.8 golang mysql@5.7 mitmproxy

# tools
brew install neovim screenfetch tig the_silver_searcher tree p7zip cloc shfmt

