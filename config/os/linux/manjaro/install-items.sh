#!/bin/bash

echo 'Install basic tools...'
sudo pacman -S go cmake neovim

sudo rm /usr/bin/vi
sudo ln -s /usr/bin/nvim /usr/bin/vi
sudo ln -s /usr/bin/nvim /usr/bin/vim

# echo 'Getting yay...'
# sudo pacman -S yay
# git clone https://aur.archlinux.org/yay.git

echo 'Installing basic tools using yay...'
yay -S the_silver_searcher tmux tig boxes graphviz neofetch tree tcl tk ctags xclip mitmproxy tldr fzf

echo 'Installing fonts and symbola'
yay -S noto-fonts noto-fonts-cjk noto-fonts-emoji wqy-microhei ttf-symbola nerd-fonts-meslo ttf-monaco
