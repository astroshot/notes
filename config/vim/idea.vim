"
" ██    ██ ██ ███    ███     ██ ███████      █████  ██     ██ ███████ ███████  ██████  ███    ███ ███████ 
" ██    ██ ██ ████  ████     ██ ██          ██   ██ ██     ██ ██      ██      ██    ██ ████  ████ ██      
" ██    ██ ██ ██ ████ ██     ██ ███████     ███████ ██  █  ██ █████   ███████ ██    ██ ██ ████ ██ █████
"  ██  ██  ██ ██  ██  ██     ██      ██     ██   ██ ██ ███ ██ ██           ██ ██    ██ ██  ██  ██ ██
"   ████   ██ ██      ██     ██ ███████     ██   ██  ███ ███  ███████ ███████  ██████  ██      ██ ███████ 
"

" Common Settings
set nu
set rnu
let mapleader = ","
nnoremap ; :
set clipboard+=unnamed

" Emulated Vim Plugins
set surround

" Easy Motion Based on KJump
nmap <leader><leader>s :action KJumpAction<cr>
nmap <leader><leader>w :action KJumpAction.Word0<cr>
nmap <leader><leader>j :action KJumpAction.Line<cr>

" IDEA Settings
set ideajoin
