<#
.SYNOPSIS
  Get current git branch.
#>
function Get-Git-CurrentBranch {
  git symbolic-ref --quiet HEAD *> $null

  if ($LASTEXITCODE -eq 0) {
    return git rev-parse --abbrev-ref HEAD
  }
  else {
    return
  }
}

function Remove-Alias ([string] $AliasName) {
  while (Test-Path Alias:$AliasName) {
    Remove-Item Alias:$AliasName -Force 2> $null
  }
}

function Format-AliasDefinition {
  param (
    [Parameter(Mandatory = $true)][string] $Definition
  )

  $definitionLines = $Definition.Trim() -split "`n" | ForEach-Object {
    $line = $_.TrimEnd()

    # Trim 1 indent
    if ($_ -match "^`t") {
      return $line.Substring(1)
    }
    elseif ($_ -match '^    ') {
      return $line.Substring(4)
    }

    return $line

  }

  return $definitionLines -join "`n"
}

<#
.SYNOPSIS
  Get git aliases' definition.
.DESCRIPTION
  Get definition of all git aliases or specific alias.
.EXAMPLE
  PS C:\> Get-Git-Aliases
  Get definition of all git aliases.
.EXAMPLE
  PS C:\> Get-Git-Aliases -Alias gst
  Get definition of `gst` alias.
#>
function Get-Git-Aliases ([string] $Alias) {
  $esc = [char] 27
  $green = 32
  $magenta = 35

  $Alias = $Alias.Trim()
  $blacklist = @(
    'Get-Git-CurrentBranch',
    'Remove-Alias',
    'Format-AliasDefinition',
    'Get-Git-Aliases'
  )
  $aliases = Get-Command -Module git-aliases | Where-Object { $_ -notin $blacklist }

  if (-not ([string]::IsNullOrEmpty($Alias))) {
    $foundAliases = $aliases | Where-Object -Property Name -Value $Alias -EQ

    if ($foundAliases -is [array]) {
      return Format-AliasDefinition($foundAliases[0].Definition)
    }
    else {
      return Format-AliasDefinition($foundAliases.Definition)
    }
  }

  $aliases = $aliases | ForEach-Object {
    $name = $_.Name
    $definition = Format-AliasDefinition($_.Definition)
    $definition = "$definition`n" # Add 1 line break for some row space

    return [PSCustomObject]@{
      Name       = $name
      Definition = $definition
    }
  }

  $cols = @(
    @{
      Name       = 'Name'
      Expression = {
        # Print alias name in green
        "$esc[$($green)m$($_.Name)$esc[0m"
      }
    },
    @{
      Name       = 'Definition'
      Expression = {
        # Print alias definition in yellow
        "$esc[$($magenta)m$($_.Definition)$esc[0m"
      }
    }
  )

  return Format-Table -InputObject $aliases -AutoSize -Wrap -Property $cols
}

# Usage: In powershell, type `notepad $profile` to edit config file, and paste the following content.
# import posh-git
Import-Module posh-git

# import oh-my-posh
Import-Module oh-my-posh

Set-Theme Powerlevel10k-Lean

# Remove conflict aliases
# Remove-Alias gc
# Remove-Alias gcb
# Remove-Alias gcm
# Remove-Alias gcs
# Remove-Alias gl
# Remove-Alias gm
# Remove-Alias gp
# Remove-Alias gpv

# Define git aliases referring to git plugin of oh-my-zsh.

function g {
  git $args
}

function ga {
  git add $args
}

function gaa {
  git add --all $args
}

function gapa {
  git add --patch $args
}

function gau {
  git add --update $args
}

function gb {
  git branch $args
}

function gba {
  git branch -a $args
}

function gbda {
  $MergedBranchs = $(git branch --merged | Select-String "^(\*|\s*(master|develop|dev)\s*$)" -NotMatch).Line
  $MergedBranchs | ForEach-Object {
    if ([string]::IsNullOrEmpty($_)) {
      return
    }
    git branch -d $_.Trim()
  }
}

function gbl {
  git blame -b -w $args
}

function gbnm {
  git branch --no-merged $args
}

function gbr {
  git branch --remote $args
}

function gbs {
  git bisect $args
}

function gbsb {
  git bisect bad $args
}

function gbsg {
  git bisect good $args
}

function gbsr {
  git bisect reset $args
}

function gbss {
  git bisect start $args
}

function gc {
  git commit -v $args
}

function gc! {
  git commit -v --amend $args
}

function gca {
  git commit -v -a $args
}

function gcam {
  git commit -a -m $args
}

function gca! {
  git commit -v -a --amend $args
}

function gcan! {
  git commit -v -a -s --no-edit --amend $args
}

function gcb {
  git checkout -b $args
}

function gcf {
  git config --list $args
}

function gcl {
  git clone --recursive $args
}

function gclean {
  git clean -df $args
}

function gcm {
  git checkout master $args
}

function gcd {
  git checkout develop $args
}

function gcmsg {
  git commit -m $args
}

function gco {
  git checkout $args
}

function gcount {
  git shortlog -sn $args
}

function gcp {
  git cherry-pick $args
}

function gcpa {
  git cherry-pick --abort $args
}

function gcpc {
  git cherry-pick --continue $args
}

function gcs {
  git commit -S $args
}

function gd {
  git diff $args
}

function gdca {
  git diff --cached $args
}

function gdt {
  git diff-tree --no-commit-id --name-only -r $args
}

function gdw {
  git diff --word-diff $args
}
function gf {
  git fetch $args
}

function gfa {
  git fetch --all --prune $args
}

function gfo {
  git fetch origin $args
}

function gg {
  git gui citool $args
}

function gga {
  git gui citool --amend $args
}

function ggf {
  $CurrentBranch = Get-Git-CurrentBranch

  git push --force origin $CurrentBranch
}

function ggfl {
  $CurrentBranch = Get-Git-CurrentBranch
  git push --force-with-lease origin $CurrentBranch
}

function ghh {
  git help $args
}

function ggsup {
  $CurrentBranch = Get-Git-CurrentBranch
  git branch --set-upstream-to=origin/$CurrentBranch
}

function gpsup {
  $CurrentBranch = Get-Git-CurrentBranch
  git push --set-upstream origin $CurrentBranch
}

function gignore {
  git update-index --assume-unchanged $args
}

function gignored {
  git ls-files -v | Select-String "^[a-z]" -CaseSensitive
}

# function gl {
# git pull $args
# }

function glg {
  git log --stat --color $args
}

function glgg {
  git log --graph --color $args
}

function glgga {
  git log --graph --decorate --all $args
}

function glgm {
  git log --graph --max-count=10 $args
}

function glgp {
  git log --stat --color -p $args
}

function glo {
  git log --oneline --decorate --color $args
}

function glog {
  git log --oneline --decorate --color --graph $args
}

function glol {
  git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit $args
}

function glola {
  git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --all $args
}

# function gm {
# 	git merge $args
# }

function gmom {
  git merge origin/master $args
}

function gmt {
  git mergetool --no-prompt $args
}

function gmtvim {
  git mergetool --no-prompt --tool=vimdiff $args
}

function gmum {
  git merge upstream/master $args
}

# function gp {
# 	git push $args
# }

function gpd {
  git push --dry-run $args
}

function gpf {
  git push --force-with-lease $args
}

function gpf! {
  git push --force $args
}

function gpoat {
  git push origin --all
  git push origin --tags
}

function gpristine {
  git reset --hard
  git clean -dfx
}

function gpu {
  git push upstream $args
}

# function gpv {
# 	git push -v $args
# }

function gr {
  git remote $args
}

function gra {
  git remote add $args
}

function grb {
  git rebase $args
}

function grba {
  git rebase --abort $args
}

function grbc {
  git rebase --continue $args
}

function grbi {
  git rebase -i $args
}

function grbm {
  git rebase master $args
}

function grbs {
  git rebase --skip $args
}

function grh {
  git reset $args
}

function grhh {
  git reset --hard $args
}

function grmv {
  git remote rename $args
}

function grrm {
  git remote remove $args
}

function grset {
  git remote set-url $args
}

function grt {
  try {
    $RootPath = git rev-parse --show-toplevel
  }
  catch {
    $RootPath = "."
  }
  Set-Location $RootPath
}

function gru {
  git reset -- $args
}

function grup {
  git remote update $args
}

function grv {
  git remote -v $args
}

function gsb {
  git status -sb $args
}

function gsd {
  git svn dcommit $args
}

function gsh {
  git show $args
}

function gsi {
  git submodule init $args
}

function gsps {
  git show --pretty=short --show-signature $args
}

function gsr {
  git svn rebase $args
}

function gss {
  git status -s $args
}

function gst {
  git status $args
}
function gsta {
  git stash save $args
}

function gstaa {
  git stash apply $args
}

function gstd {
  git stash drop $args
}

function gstl {
  git stash list $args
}

function gstp {
  git stash pop $args
}

function gstc {
  git stash clear $args
}

function gsts {
  git stash show --text $args
}

function gsu {
  git submodule update $args
}

function gts {
  git tag -s $args
}

function gunignore {
  git update-index --no-assume-unchanged $args
}

function gunwip {
  Write-Output $(git log -n 1 | Select-String "--wip--" -Quiet).Count
  git reset HEAD~1
}

function gup {
  git pull --rebase $args
}

function gupv {
  git pull --rebase -v $args
}

function glum {
  git pull upstream master $args
}

function gvt {
  git verify-tag $args
}

function gwch {
  git whatchanged -p --abbrev-commit --pretty=medium $args
}

function gwip {
  git add -A
  git rm $(git ls-files --deleted) 2> $null
  git commit --no-verify -m "--wip-- [skip ci]"
}

function ggl {
  $CurrentBranch = Get-Git-CurrentBranch
  git pull origin $CurrentBranch
}

function ggpush {
  $CurrentBranch = Get-Git-CurrentBranch
  git push origin $CurrentBranch
}

# Set-Alias

# Set-Alias g 'git'
# Set-Alias ga 'git add'
# Set-Alias gaa 'git add --all'
# Set-Alias gapa 'git add --patch'
# Set-Alias gau 'git add --update'
# Set-Alias gav 'git add --verbose'
# Set-Alias gap 'git apply'
# Set-Alias gapt 'git apply --3way'
#
# Set-Alias gb 'git branch'
# Set-Alias gba 'git branch -a'
# Set-Alias gbd 'git branch -d'
# Set-Alias gbda 'git branch --no-color --merged | command grep -vE "^(\+|\*|\s*(master|development|develop|devel|dev)\s*$)" | command xargs -n 1 git branch -d'
# Set-Alias gbD 'git branch -D'
# Set-Alias gbl 'git blame -b -w'
# Set-Alias gbnm 'git branch --no-merged'
# Set-Alias gbr 'git branch --remote'
# Set-Alias gbs 'git bisect'
# Set-Alias gbsb 'git bisect bad'
# Set-Alias gbsg 'git bisect good'
# Set-Alias gbsr 'git bisect reset'
# Set-Alias gbss 'git bisect start'
#
# Set-Alias gc 'git commit -v'
# Set-Alias gc! 'git commit -v --amend'
# Set-Alias gcn! 'git commit -v --no-edit --amend'
# Set-Alias gca 'git commit -v -a'
# Set-Alias gca! 'git commit -v -a --amend'
# Set-Alias gcan! 'git commit -v -a --no-edit --amend'
# Set-Alias gcans! 'git commit -v -a -s --no-edit --amend'
# Set-Alias gcam 'git commit -a -m'
# Set-Alias gcsm 'git commit -s -m'
# Set-Alias gcb 'git checkout -b'
# Set-Alias gcf 'git config --list'
# Set-Alias gcl 'git clone --recurse-submodules'
# Set-Alias gclean 'git clean -id'
# Set-Alias gpristine 'git reset --hard && git clean -dffx'
# Set-Alias gcm 'git checkout master'
# Set-Alias gcd 'git checkout develop'
# Set-Alias gcmsg 'git commit -m'
# Set-Alias gco 'git checkout'
# Set-Alias gcount 'git shortlog -sn'
# Set-Alias gcp 'git cherry-pick'
# Set-Alias gcpa 'git cherry-pick --abort'
# Set-Alias gcpc 'git cherry-pick --continue'
# Set-Alias gcs 'git commit -S'
#
# Set-Alias gd 'git diff'
# Set-Alias gdca 'git diff --cached'
# Set-Alias gdcw 'git diff --cached --word-diff'
# Set-Alias gdct 'git describe --tags $(git rev-list --tags --max-count 1)'
# Set-Alias gds 'git diff --staged'
# Set-Alias gdt 'git diff-tree --no-commit-id --name-only -r'
# Set-Alias gdw 'git diff --word-diff'
