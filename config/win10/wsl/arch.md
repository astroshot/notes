# Archlinux WSL2 Config

## Fix key error

After installation, default user is root.

```bash
pacman-key --init
pacman-key --populate archlinux
pacman-key --refresh-keys
```
