#!/bin/bash

# run with root

pacman-key --init
pacman-key --populate archlinux
pacman-key --refresh-keys

pacman -Syyu

pacman -Rsc vim

pacman -S neovim neofetch base-devel zsh

# python
pacman -S python python-pip python-virtualenvwrapper

# users
username=altair
useradd -m ${username}
chsh -s /bin/zsh ${username}
groupadd sudo
usermod -aG sudo ${username}
