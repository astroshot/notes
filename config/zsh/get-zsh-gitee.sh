#!/bin/bash

echo 'Getting oh-my-zsh...'
git clone https://gitee.com/mirrors/oh-my-zsh.git ~/.oh-my-zsh

# echo 'Downloading powerlevel9k...'
# git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

# echo 'Getting powerlevel10k'
# git clone https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh}/themes/powerlevel10k

echo 'Getting zsh-highlighting...'
git clone https://gitee.com/mirror-github/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

echo 'Getting zsh-autosuggestions...'
git clone https://gitee.com/mirror-github/zsh-autosuggestions.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
