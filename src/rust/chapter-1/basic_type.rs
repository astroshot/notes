fn main() {
    // tuple could be depackaged by operator .
    let x: (i32, f64, u8) = (300, 3.1, 24);
    let i1 = x.0;
    let f2 = x.1;
    let u3 = x.2;

    println!("{}, {}, {}", i1, f2, u3);
}
