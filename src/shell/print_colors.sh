#!/bin/bash

# Due to some reason, `/bin/sh` is a soft link of `/bin/dash` in Debian buster.
# So execution may enconter error by just typing `sh print_colors.sh`.

for ((i=16; i<256; i++)); do
    printf "\e[48;5;${i}m%03d" $i;
    printf '\e[0m';
    [ ! $((($i - 15) % 6)) -eq 0  ] && printf ' ' || printf '\n'
done

