REM 打印目录中所有一级子目录中的 mp3 文件列表

@echo off
set mypath=F:\
rem cd %mypath%
echo begining...

for /d %%i in (*) do (
  echo %%i
  cd %%i
  dir /b *.mp3 > mp3.txt
  cd ..
)