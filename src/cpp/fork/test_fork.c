#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


int main(int argc, char** argv) {
    pid_t pid;

    pid = fork();
    if (pid == 0) {
        // child process
        printf("This is Child, pid: %d\n", pid);
    } else if (pid > 0) {
        // parent process
        printf("This is father, pid: %d\n", pid);
    } else {
        printf("fork err!\n");
        exit(0);
    }

    pid = fork();
    if (pid == 0) {
        printf("This is Child, pid: %d\n", pid);
    } else if (pid > 0) {
        printf("This is father, pid: %d\n", pid);
    } else {
        printf("fork err!\n");
        exit(0);
    }
    return 0;

}
