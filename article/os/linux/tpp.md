# Text Powerpoint

## Installation On Debian

```bash
sudo apt-get install libncurses5-dev libncursesw5-dev
sudo gem install ncurses-ruby
```


## Usage

```tpp
--fgcolor black
--bgcolor white
--title FG/BG tests
--author Andreas Krennmair
--date today

This is the abstract. Bla, bla.
--newpage
--heading bla bla bla

  * bla

  * bla
---
--beginslideleft

  * bla
--endslideleft
--beginslidetop

  * bla
--endslidetop

more bla bla bla.
```
