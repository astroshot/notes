# git 配置

## 多账号配置

### 配置密钥

按邮箱生成密钥

```bash
ssh-keygen -t rsa -f ~/.ssh/id_rsa_your_custom_name -C "your email"
```

添加密钥

```bash
ssh-add ~/.ssh/id_rsa_your_custom_name
```

编辑 `~/.ssh/config` 文件，添加如下内容：

```text
Host github.com
    HostName github.com
    User Your_Custom_name
    IdentityFile ~/.ssh/id_rsa_your_custom_name
```

### 配置用户信息

cd 到项目的根目录，查看 git 配置：

```bash
git config --list
```

看一下 `user.name` 和 `user.email` 是否和希望上传的账号匹配，否则使用如下命令进行修改：

```bash
git config user.email "your@email.com"
git config user.name "your name"
```

### 配置多用户信息

File `~/.ssh/config` is also very useful when you have to work with multiple git accounts.

```config
Host github.com
    HostName github.com
    User git
    IdentityFile ~/.ssh/github

Host github_as_alice
    HostName github.com
    User git
    IdentityFile ~/.ssh/github_alice

Host github_as_bob
    HostName github.com
    User git
    IdentityFile ~/.ssh/github_bob
```

Default `github.com` will work by default.

For alice: replace 'git@github.com' with 'git@github\_as\_alice' when you use `git clone` or `git remote add origin`.

For bob: replace 'git@github.com' with 'git@github\_as\_bob' when you use `git clone` or `git remote add origin`.

## 跨平台提交代码配置

在 windows 系统中设置

```bash
git config --global core.autocrlf input
git config --global core.safecrlf true
git config --global core.eol lf
```
